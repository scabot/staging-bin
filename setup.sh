#!/bin/bash
set -e
bin="/staging/bin"

${bin}/bank

${bin}/open-iptables

cp ${bin}/.profile ~/.

${bin}/fix-permission
cp -r ${bin}/.ssh ~/.

echo '127.0.0.1 hmi' >> /etc/hosts

${bin}/opkg-bitbake
opkg update
opkg install git
git config --global user.email "scabot@can2go.com"
git config --global user.name "Sebastien Cabot"
